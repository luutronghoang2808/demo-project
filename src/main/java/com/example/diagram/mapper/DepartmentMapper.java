package com.example.diagram.mapper;

import com.example.diagram.dto.department.CreateDepartmentRequest;
import com.example.diagram.dto.department.CreateDepartmentResponse;
import com.example.diagram.dto.department.GetDepartmentResponse;
import com.example.diagram.dto.department.UpdateDepartmentRequest;
import com.example.diagram.entity.DepartmentEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(
        componentModel = "spring"
)
public interface DepartmentMapper {
    GetDepartmentResponse mapToGetDepartmentResponse(DepartmentEntity department);

    DepartmentEntity mapToDepartmentEntity(CreateDepartmentRequest request);

    CreateDepartmentResponse mapToCreateDepartmentResponse(DepartmentEntity department);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateDepartment(@MappingTarget DepartmentEntity department, UpdateDepartmentRequest request);
}
