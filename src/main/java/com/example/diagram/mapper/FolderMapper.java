package com.example.diagram.mapper;

import com.example.diagram.dto.folder.CreateFolderRequest;
import com.example.diagram.dto.folder.CreateFolderResponse;
import com.example.diagram.dto.folder.GetFolderResponse;
import com.example.diagram.dto.folder.UpdateFolderRequest;
import com.example.diagram.entity.FolderEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(
        componentModel = "spring"
)
public interface FolderMapper {

    GetFolderResponse mapToGetFolderResponse(FolderEntity folder);

    FolderEntity mapToFolderEntity(CreateFolderRequest request);

    CreateFolderResponse mapToCreateFolderResponse(FolderEntity folder);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateFolder(@MappingTarget FolderEntity folder, UpdateFolderRequest request);
}
