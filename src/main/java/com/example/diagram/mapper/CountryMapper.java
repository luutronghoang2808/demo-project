package com.example.diagram.mapper;

import com.example.diagram.dto.country.CreateCountryRequest;
import com.example.diagram.dto.country.CreateCountryResponse;
import com.example.diagram.dto.country.GetCountryResponse;
import com.example.diagram.dto.country.UpdateCountryRequest;
import com.example.diagram.entity.CountryEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(
        componentModel = "spring"
)
public interface CountryMapper {
    GetCountryResponse mapToGetCountryResponse(CountryEntity country);

    CountryEntity mapToCountryEntity(CreateCountryRequest request);

    CreateCountryResponse mapToCreateCountryResponse(CountryEntity country);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateCountry(@MappingTarget CountryEntity country, UpdateCountryRequest request);
}
