package com.example.diagram.mapper;

import com.example.diagram.dto.employee.CreateEmployeeRequest;
import com.example.diagram.dto.employee.CreateEmployeeResponse;
import com.example.diagram.dto.employee.GetEmployeeResponse;
import com.example.diagram.dto.employee.UpdateEmployeeRequest;
import com.example.diagram.entity.EmployeeEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(
        componentModel = "spring"
)
public interface EmployeeMapper {
    GetEmployeeResponse mapToGetEmployeeResponse(EmployeeEntity employee);

    EmployeeEntity mapToEmployeeEntity(CreateEmployeeRequest request);

    CreateEmployeeResponse mapToCreateEmployeeResponse(EmployeeEntity employee);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateEmployee(@MappingTarget EmployeeEntity employee, UpdateEmployeeRequest request);
}
