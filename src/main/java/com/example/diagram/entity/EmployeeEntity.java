package com.example.diagram.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "employee")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "empname")
    private String empName;
    @Column(name = "email")
    private String email;
    @Column(name = "deptid")
    private Integer deptId;
    @Column(name = "folderid")
    private Integer folderId;
}
