package com.example.diagram.dto.country;

import lombok.Data;

@Data
public class CreateCountryRequest {
    private String countryName;
}
