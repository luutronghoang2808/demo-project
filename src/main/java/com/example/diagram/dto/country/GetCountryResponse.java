package com.example.diagram.dto.country;

import lombok.Data;

@Data
public class GetCountryResponse {
    private Long id;
    private String countryName;
}
