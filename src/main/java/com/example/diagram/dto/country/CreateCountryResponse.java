package com.example.diagram.dto.country;

import lombok.Data;

@Data
public class CreateCountryResponse {
    private Long id;
}
