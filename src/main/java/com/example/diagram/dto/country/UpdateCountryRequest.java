package com.example.diagram.dto.country;

import lombok.Data;

@Data
public class UpdateCountryRequest {
    private String countryName;
}
