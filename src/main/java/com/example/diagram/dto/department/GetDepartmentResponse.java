package com.example.diagram.dto.department;

import lombok.Data;

@Data
public class GetDepartmentResponse {
    private Long id;
    private String deptName;
    private Integer countryId;
}
