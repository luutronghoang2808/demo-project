package com.example.diagram.dto.department;

import lombok.Data;

@Data
public class CreateDepartmentRequest {
    private String deptName;
    private Integer countryId;
}
