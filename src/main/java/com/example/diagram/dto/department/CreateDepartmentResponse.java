package com.example.diagram.dto.department;

import lombok.Data;

@Data
public class CreateDepartmentResponse {
    private Long id;
}
