package com.example.diagram.dto.department;

import lombok.Data;

@Data
public class UpdateDepartmentRequest {
    private String deptName;
    private Integer countryId;
}
