package com.example.diagram.dto.employee;

import lombok.Data;

@Data
public class CreateEmployeeRequest {
    private String empName;
    private String email;
    private Integer deptId;
    private Integer folderId;
}
