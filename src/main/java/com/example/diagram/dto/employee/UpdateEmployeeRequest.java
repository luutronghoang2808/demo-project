package com.example.diagram.dto.employee;

import lombok.Data;

@Data
public class UpdateEmployeeRequest {
    private String empName;
    private String email;
    private Integer deptId;
    private Integer folderId;
}
