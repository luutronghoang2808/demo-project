package com.example.diagram.dto.employee;

import lombok.Data;

@Data
public class CreateEmployeeResponse {
    private Long id;

}
