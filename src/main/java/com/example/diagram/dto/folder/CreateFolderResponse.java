package com.example.diagram.dto.folder;

import lombok.Data;

@Data
public class CreateFolderResponse {
    private Long id;
}
