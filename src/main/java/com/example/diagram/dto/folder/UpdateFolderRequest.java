package com.example.diagram.dto.folder;

import lombok.Data;

@Data
public class UpdateFolderRequest {
    private Long id;
    private String folderName;
    private String accessType;
}
