package com.example.diagram.dto.folder;

import lombok.Data;

@Data
public class CreateFolderRequest {
    private String folderName;
    private String accessType;
}
