package com.example.diagram.controller;

import com.example.diagram.dto.folder.CreateFolderRequest;
import com.example.diagram.dto.folder.CreateFolderResponse;
import com.example.diagram.dto.folder.GetFolderResponse;
import com.example.diagram.dto.folder.UpdateFolderRequest;
import com.example.diagram.service.folder.FolderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/folder")
public class FolderController {
    @Autowired
    private FolderService folderService;

    @GetMapping("/list")
    public List<GetFolderResponse> getAllFolder() {
        return folderService.getAllFolder();
    }

    @PostMapping("/")
    public CreateFolderResponse createFolder(@RequestBody CreateFolderRequest request) {
        return folderService.createFolder(request);
    }

    @GetMapping("/{id}")
    public GetFolderResponse getFolder(@PathVariable(name = "id") Long id) {
        return folderService.getFolder(id);
    }

    @PutMapping("/{id}")
    public void updateFolder(@PathVariable(name = "id") Long id, @RequestBody UpdateFolderRequest request) {
        folderService.updateFolder(id, request);
    }

    @DeleteMapping("/{id}")
    public void deleteFolder(@PathVariable(name = "id") Long id) {
        folderService.deleteFolder(id);
    }
}
