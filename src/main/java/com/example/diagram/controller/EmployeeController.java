package com.example.diagram.controller;

import com.example.diagram.dto.employee.CreateEmployeeRequest;
import com.example.diagram.dto.employee.CreateEmployeeResponse;
import com.example.diagram.dto.employee.GetEmployeeResponse;
import com.example.diagram.dto.employee.UpdateEmployeeRequest;
import com.example.diagram.service.employee.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/list")
    public List<GetEmployeeResponse> getAllEmployee() {
        return employeeService.getAllEmployee();
    }

    @PostMapping("/")
    public CreateEmployeeResponse createEmployee(@RequestBody CreateEmployeeRequest request) {
        return employeeService.createEmployee(request);
    }

    @GetMapping("/{id}")
    public GetEmployeeResponse getEmployee(@PathVariable(name = "id") Long id) {
        return employeeService.getEmployee(id);
    }


    @PutMapping("/{id}")
    public void updateEmployee(@PathVariable(name = "id") Long id, @RequestBody UpdateEmployeeRequest request) {
        employeeService.updateEmployee(id, request);
    }

    @DeleteMapping("/{id}")
    public void deleteEmployee(@PathVariable(name = "id") Long id) {
        employeeService.deleteEmployee(id);
    }

}
