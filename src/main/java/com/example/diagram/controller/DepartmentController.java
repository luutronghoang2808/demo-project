package com.example.diagram.controller;

import com.example.diagram.dto.department.CreateDepartmentRequest;
import com.example.diagram.dto.department.CreateDepartmentResponse;
import com.example.diagram.dto.department.GetDepartmentResponse;
import com.example.diagram.dto.department.UpdateDepartmentRequest;
import com.example.diagram.service.department.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/department")
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;

    @GetMapping("/list")
    public List<GetDepartmentResponse> getAllDepartment() {
        return departmentService.getAllDepartment();
    }

    @PostMapping("/")
    public CreateDepartmentResponse createDepartment(@RequestBody CreateDepartmentRequest request) {
        return departmentService.createDepartment(request);
    }

    @GetMapping("/{id}")
    public GetDepartmentResponse getDepartment(@PathVariable(name = "id") Long id) {
        return departmentService.getDepartment(id);
    }

    @PutMapping("/{id}")
    public void updateDepartment(@PathVariable(name = "id") Long id, @RequestBody UpdateDepartmentRequest request) {
        departmentService.updateDepartment(id, request);
    }

    @DeleteMapping("/{id}")
    public void deleteDepartment(@PathVariable(name = "id") Long id) {
        departmentService.deleteDepartment(id);
    }
}
