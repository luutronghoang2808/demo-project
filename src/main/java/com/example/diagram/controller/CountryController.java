package com.example.diagram.controller;

import com.example.diagram.dto.country.CreateCountryRequest;
import com.example.diagram.dto.country.CreateCountryResponse;
import com.example.diagram.dto.country.GetCountryResponse;
import com.example.diagram.dto.country.UpdateCountryRequest;
import com.example.diagram.service.country.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/country")
public class CountryController {
    @Autowired
    private CountryService countryService;

    @GetMapping("/list")
    public List<GetCountryResponse> getAllCountry() {
        return countryService.getAllCountry();
    }

    @PostMapping("/")
    public CreateCountryResponse createCountry(@RequestBody CreateCountryRequest request) {
        return countryService.createCountry(request);
    }

    @GetMapping("/{id}")
    public GetCountryResponse getCountry(@PathVariable(name = "id") Long id) {
        return countryService.getCountry(id);
    }

    @PutMapping("/{id}")
    public void updateCountry(@PathVariable(name = "id") Long id, @RequestBody UpdateCountryRequest request) {
        countryService.updateCountry(id, request);
    }

    @DeleteMapping("/{id}")
    public void deleteCountry(@PathVariable(name = "id") Long id) {
        countryService.deleteCountry(id);
    }
}
