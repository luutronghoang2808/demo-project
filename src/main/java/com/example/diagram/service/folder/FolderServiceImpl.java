package com.example.diagram.service.folder;

import com.example.diagram.dto.folder.CreateFolderRequest;
import com.example.diagram.dto.folder.CreateFolderResponse;
import com.example.diagram.dto.folder.GetFolderResponse;
import com.example.diagram.dto.folder.UpdateFolderRequest;
import com.example.diagram.entity.FolderEntity;
import com.example.diagram.mapper.FolderMapper;
import com.example.diagram.repository.FolderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FolderServiceImpl implements FolderService {
    @Autowired
    private FolderRepository folderRepository;
    @Autowired
    private FolderMapper folderMapper;

    @Override
    public List<GetFolderResponse> getAllFolder() {
        List<FolderEntity> list = folderRepository.findAll();
        return list.stream().map(folderMapper::mapToGetFolderResponse).collect(Collectors.toList());
    }

    @Override
    public CreateFolderResponse createFolder(CreateFolderRequest request) {
        FolderEntity folder = folderMapper.mapToFolderEntity(request);
        folderRepository.save(folder);
        return folderMapper.mapToCreateFolderResponse(folder);
    }

    @Override
    public GetFolderResponse getFolder(Long id) {
        FolderEntity folder = folderRepository.findById(id).orElse(null);
        return folderMapper.mapToGetFolderResponse(folder);
    }

    @Override
    public void updateFolder(Long id, UpdateFolderRequest request) {
        FolderEntity folder = folderRepository.findById(id).orElse(null);
        folderMapper.updateFolder(folder, request);
    }

    @Override
    public void deleteFolder(Long id) {
        FolderEntity folder = folderRepository.findById(id).orElse(null);
        folderRepository.delete(folder);
    }
}
