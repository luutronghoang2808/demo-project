package com.example.diagram.service.folder;

import com.example.diagram.dto.folder.CreateFolderRequest;
import com.example.diagram.dto.folder.CreateFolderResponse;
import com.example.diagram.dto.folder.GetFolderResponse;
import com.example.diagram.dto.folder.UpdateFolderRequest;

import java.util.List;

public interface FolderService {
    List<GetFolderResponse> getAllFolder();

    CreateFolderResponse createFolder(CreateFolderRequest request);

    GetFolderResponse getFolder(Long id);

    void updateFolder(Long id, UpdateFolderRequest request);

    void deleteFolder(Long id);
}
