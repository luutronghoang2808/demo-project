package com.example.diagram.service.employee;

import com.example.diagram.dto.employee.CreateEmployeeRequest;
import com.example.diagram.dto.employee.CreateEmployeeResponse;
import com.example.diagram.dto.employee.GetEmployeeResponse;
import com.example.diagram.dto.employee.UpdateEmployeeRequest;

import java.util.List;

public interface EmployeeService {
    List<GetEmployeeResponse> getAllEmployee();

    CreateEmployeeResponse createEmployee(CreateEmployeeRequest request);

    GetEmployeeResponse getEmployee(Long id);

    void updateEmployee(Long id, UpdateEmployeeRequest request);

    void deleteEmployee(Long id);
}
