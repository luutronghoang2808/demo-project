package com.example.diagram.service.employee;

import com.example.diagram.dto.employee.CreateEmployeeRequest;
import com.example.diagram.dto.employee.CreateEmployeeResponse;
import com.example.diagram.dto.employee.GetEmployeeResponse;
import com.example.diagram.dto.employee.UpdateEmployeeRequest;
import com.example.diagram.entity.EmployeeEntity;
import com.example.diagram.entity.FolderEntity;
import com.example.diagram.mapper.EmployeeMapper;
import com.example.diagram.repository.EmployeeRepository;
import com.example.diagram.repository.FolderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private FolderRepository folderRepository;
    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public List<GetEmployeeResponse> getAllEmployee() {
        List<EmployeeEntity> list = employeeRepository.findAll();
        return list.stream().map(employeeMapper::mapToGetEmployeeResponse).collect(Collectors.toList());
    }

    @Override
    public CreateEmployeeResponse createEmployee(CreateEmployeeRequest request) {
        EmployeeEntity employee = employeeMapper.mapToEmployeeEntity(request);
        employeeRepository.save(employee);
        return employeeMapper.mapToCreateEmployeeResponse(employee);
    }

    @Override
    public GetEmployeeResponse getEmployee(Long id) {
        EmployeeEntity employee = employeeRepository.findById(id).orElse(null);
        return employeeMapper.mapToGetEmployeeResponse(employee);
    }


    @Override
    public void updateEmployee(Long id, UpdateEmployeeRequest request) {
        EmployeeEntity employee = employeeRepository.findById(id).orElse(null);
        employeeMapper.updateEmployee(employee, request);
    }

    @Override
    public void deleteEmployee(Long id) {
        EmployeeEntity employee = employeeRepository.findById(id).orElse(null);
        employeeRepository.delete(employee);
    }
}
