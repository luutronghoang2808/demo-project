package com.example.diagram.service.country;

import com.example.diagram.dto.country.CreateCountryRequest;
import com.example.diagram.dto.country.CreateCountryResponse;
import com.example.diagram.dto.country.GetCountryResponse;
import com.example.diagram.dto.country.UpdateCountryRequest;

import java.util.List;

public interface CountryService {
    List<GetCountryResponse> getAllCountry();

    CreateCountryResponse createCountry(CreateCountryRequest request);

    GetCountryResponse getCountry(Long id);

    void updateCountry(Long id, UpdateCountryRequest request);

    void deleteCountry(Long id);
}
