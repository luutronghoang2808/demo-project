package com.example.diagram.service.country;

import com.example.diagram.dto.country.CreateCountryRequest;
import com.example.diagram.dto.country.CreateCountryResponse;
import com.example.diagram.dto.country.GetCountryResponse;
import com.example.diagram.dto.country.UpdateCountryRequest;
import com.example.diagram.entity.CountryEntity;
import com.example.diagram.mapper.CountryMapper;
import com.example.diagram.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CountryServiceImpl implements CountryService {
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private CountryMapper countryMapper;

    @Override
    public List<GetCountryResponse> getAllCountry() {
        List<CountryEntity> list = countryRepository.findAll();
        return list.stream().map(countryMapper::mapToGetCountryResponse).collect(Collectors.toList());
    }

    @Override
    public CreateCountryResponse createCountry(CreateCountryRequest request) {
        CountryEntity country = countryMapper.mapToCountryEntity(request);
        countryRepository.save(country);
        return countryMapper.mapToCreateCountryResponse(country);
    }

    @Override
    public GetCountryResponse getCountry(Long id) {
        CountryEntity country = countryRepository.findById(id).orElse(null);
        return countryMapper.mapToGetCountryResponse(country);
    }

    @Override
    public void updateCountry(Long id, UpdateCountryRequest request) {
        CountryEntity country = countryRepository.findById(id).orElse(null);
        countryMapper.updateCountry(country, request);
    }

    @Override
    public void deleteCountry(Long id) {
        CountryEntity country = countryRepository.findById(id).orElse(null);
        countryRepository.delete(country);
    }
}
