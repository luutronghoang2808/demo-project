package com.example.diagram.service.department;

import com.example.diagram.dto.department.CreateDepartmentRequest;
import com.example.diagram.dto.department.CreateDepartmentResponse;
import com.example.diagram.dto.department.GetDepartmentResponse;
import com.example.diagram.dto.department.UpdateDepartmentRequest;
import com.example.diagram.entity.DepartmentEntity;
import com.example.diagram.mapper.DepartmentMapper;
import com.example.diagram.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private DepartmentMapper departmentMapper;

    @Override
    public List<GetDepartmentResponse> getAllDepartment() {
        List<DepartmentEntity> list = departmentRepository.findAll();
        return list.stream().map(departmentMapper::mapToGetDepartmentResponse).collect(Collectors.toList());
    }

    @Override
    public CreateDepartmentResponse createDepartment(CreateDepartmentRequest request) {
        DepartmentEntity department = departmentMapper.mapToDepartmentEntity(request);
        departmentRepository.save(department);
        return departmentMapper.mapToCreateDepartmentResponse(department);
    }

    @Override
    public GetDepartmentResponse getDepartment(Long id) {
        DepartmentEntity department = departmentRepository.findById(id).orElse(null);
        return departmentMapper.mapToGetDepartmentResponse(department);
    }

    @Override
    public void updateDepartment(Long id, UpdateDepartmentRequest request) {
        DepartmentEntity department = departmentRepository.findById(id).orElse(null);
        departmentMapper.updateDepartment(department, request);
    }

    @Override
    public void deleteDepartment(Long id) {
        DepartmentEntity department = departmentRepository.findById(id).orElse(null);
        departmentRepository.delete(department);
    }
}
