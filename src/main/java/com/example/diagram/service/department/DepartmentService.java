package com.example.diagram.service.department;

import com.example.diagram.dto.department.CreateDepartmentRequest;
import com.example.diagram.dto.department.CreateDepartmentResponse;
import com.example.diagram.dto.department.GetDepartmentResponse;
import com.example.diagram.dto.department.UpdateDepartmentRequest;

import java.util.List;

public interface DepartmentService {
    List<GetDepartmentResponse> getAllDepartment();

    CreateDepartmentResponse createDepartment(CreateDepartmentRequest request);

    GetDepartmentResponse getDepartment(Long id);


    void updateDepartment(Long id, UpdateDepartmentRequest request);

    void deleteDepartment(Long id);
}
