create table country (
    id          serial
        primary key,
    countryName varchar(255) not null
);

create table department (
    id        serial
        primary key,
    deptName  varchar(255) not null,
    countryId integer      not null
);

create table employee (
    id       serial
        primary key,
    empName  varchar(255) not null,
    email    varchar(255) null,
    deptId   integer      not null,
    folderId integer      not null
);

create table folder (
    id         serial
        primary key,
    folderName varchar(255) not null,
    accessType varchar(255) null
)
