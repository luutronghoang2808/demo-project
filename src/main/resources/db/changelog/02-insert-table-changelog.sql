INSERT INTO country (countryName) VALUES ('Hà Nội');
INSERT INTO country (countryName) VALUES ('Thanh Hóa');
INSERT INTO country (countryName) VALUES ('Đà Nẵng');
INSERT INTO country (countryName) VALUES ('Hồ Chí Minh');
INSERT INTO country (countryName) VALUES ('Hải Phòng');

INSERT INTO department(deptName, countryId) VALUES ('Phòng Ban 1', 1);
INSERT INTO department(deptName, countryId) VALUES ('Phòng Ban 2', 2);
INSERT INTO department(deptName, countryId) VALUES ('Phòng Ban 3', 3);
INSERT INTO department(deptName, countryId) VALUES ('Phòng Ban 4', 4);
INSERT INTO department(deptName, countryId) VALUES ('Phòng Ban 5', 5);

INSERT INTO folder (folderName, accessType) VALUES ('CV1','Công Việc 1');
INSERT INTO folder (folderName, accessType) VALUES ('CV2','Công Việc 2');
INSERT INTO folder (folderName, accessType) VALUES ('CV3','Công Việc 3');
INSERT INTO folder (folderName, accessType) VALUES ('CV4','Công Việc 4');
INSERT INTO folder (folderName, accessType) VALUES ('CV5','Công Việc 5');

INSERT INTO employee (empName, email, deptId, folderId) VALUES ('Lê Thị Lan','lan@gmail.com', 1, 3);
INSERT INTO employee (empName, email, deptId, folderId) VALUES ('Lê Thị Thảo','thao@gmail.com', 2, 5);
INSERT INTO employee (empName, email, deptId, folderId) VALUES ('Lê Thị Hiền','hien@gmail.com', 3, 1);
INSERT INTO employee (empName, email, deptId, folderId) VALUES ('Lê Thị Hương','huong@gmail.com', 4, 2);
INSERT INTO employee (empName, email, deptId, folderId) VALUES ('Lê Thị Hoài','hoai@gmail.com', 5, 4);




